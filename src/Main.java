import Controllers.FamilyController;
import Entity.Family;
import Entity.Human;
import Entity.Man;


public class Main {

    private final static FamilyController familyController = new FamilyController();

    public static void main(String[] args) {

        Human test1 = new Human("TestMother", "Homework10", "18/02/1970", 99);
        System.out.println(test1.describeAge());
        System.out.println(test1.toString());
        System.out.println();
        Human test2 = new Human("TestFather", "Homework10", "14/10/1959", 99);
        System.out.println(test2.describeAge());
        System.out.println(test2.toString());
        System.out.println();

        Family testFamily = new Family(test1, test2);
        familyController.saveFamily(testFamily);
        System.out.println();

        Man test3 = new Man("TestChild", "Homework10", "28/01/1996", 99);
        familyController.adoptChild(testFamily, test3);
        System.out.println(test3.describeAge());
        System.out.println(test3.toString());
    }
}






